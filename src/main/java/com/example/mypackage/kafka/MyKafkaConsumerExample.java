package com.example.mypackage.kafka;


import com.example.mypackage.dto.KafkaMessageModel;
import com.example.mypackage.dto.UserDetailModel;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@KafkaListener(id = "consumer-1", topics = {"topic-1"})
public class MyKafkaConsumerExample {
    @KafkaHandler
    public void receive1(KafkaMessageModel model) {
        System.out.println("KAFKA: received1: " + model.toString());
    }

    @KafkaHandler
    public void receive2(UserDetailModel model) {
        System.out.println("KAFKA: received2: " + model.toString());
    }

    @KafkaHandler(isDefault = true)
    public void receiveDefault(Object obj) {
        System.out.println("KAFKA: received default: " + obj);
    }
}
