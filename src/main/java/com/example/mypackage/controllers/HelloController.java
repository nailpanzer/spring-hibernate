package com.example.mypackage.controllers;

import com.example.mypackage.database.repositories.JpaAuthorRepository;
import com.example.mypackage.dto.MessageModel;
import com.example.mypackage.database.repositories.HibAuthorRepository;
import com.example.mypackage.database.tables.AuthorTable;
import jakarta.persistence.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/hello")
public class HelloController {
    @Autowired
    HibAuthorRepository hibAuthorRepository;
    @Autowired
    JpaAuthorRepository jpaAuthorRepository;

    @GetMapping("/hello")
    @ResponseBody
    public MessageModel hello(@RequestParam(name = "name", defaultValue = "default-name") String name) {
        return new MessageModel(String.format("Hello, %s", name));
    }

    @GetMapping("/insert")
    @ResponseBody
    public MessageModel insertAuthor(@RequestParam(name = "author", defaultValue = "123") String author) {
        hibAuthorRepository.insert(new AuthorTable("HIB " + author));
        jpaAuthorRepository.save(new AuthorTable("JPA " + author));

        return new MessageModel("Done");
    }

    @GetMapping("/get")
    @ResponseBody
    public MessageModel getAuthors() {
        // List<AuthorTable> authors = hibAuthorRepository.getAll();
        List<AuthorTable> authors = jpaAuthorRepository.findAll();

        String combined = authors.stream().map(AuthorTable::getName).collect(Collectors.joining(", "));

        return new MessageModel(combined);
    }

}
