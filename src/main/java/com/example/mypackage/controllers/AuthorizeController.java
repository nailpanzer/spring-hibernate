package com.example.mypackage.controllers;

import com.example.mypackage.annotations.JwtSecured;
import com.example.mypackage.dto.LoginRequest;
import com.example.mypackage.dto.MessageModel;
import com.example.mypackage.dto.TokenModel;
import com.example.mypackage.dto.UserDetailModel;
import com.example.mypackage.services.JwtManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthorizeController {
    @Autowired
    JwtManageService jwtManageService;

    @GetMapping("/check")
    @ResponseBody
    @JwtSecured
    public MessageModel check() {
        return new MessageModel("Authorized!");
    }

    @GetMapping("/login")
    @ResponseBody
    public TokenModel login(@RequestParam(name = "name") String name) {
        String token = jwtManageService.createToken(new UserDetailModel("123", name, "my-email"));

        return new TokenModel(token);
    }

}
