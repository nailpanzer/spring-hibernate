package com.example.mypackage.controllers;


import com.example.mypackage.dto.KafkaMessageModel;
import com.example.mypackage.dto.MessageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/kafka")
public class KafkaExampleController {
    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @GetMapping("/send")
    public MessageModel send(@RequestParam(name = "msg") String msg) {
        KafkaMessageModel kafkaMessage = new KafkaMessageModel("kafka msg = " + msg);
        CompletableFuture<SendResult<String, Object>> sendFuture = kafkaTemplate.send("topic-1",
                kafkaMessage);

        System.out.println("KAFKA: sending: " + kafkaMessage.getMessage());

        try {
            sendFuture.get();
            return new MessageModel("Success");
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

}
