package com.example.mypackage.aspects;

import com.example.mypackage.abstractions.HttpSecurityStrategy;
import com.example.mypackage.dto.TokenModel;
import com.example.mypackage.exceptions.UnauthorizedException;
import com.example.mypackage.services.JwtManageService;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Map;

@Component
@Aspect
public class HttpJwtSecurityStrategy implements HttpSecurityStrategy {
    @Autowired
    JwtManageService jwtManageService;

    @Override
    public boolean checkAuth(HttpServletRequest request) {
        String token = request.getHeader("My-auth");

        if (token == null) {
            token = request.getParameter("My-auth");
        }
        if (token == null) {
            return false;
        }

        return jwtManageService.validate(token);
    }

    @Before("@annotation(com.example.mypackage.annotations.JwtSecured)")
    public void handle(JoinPoint joinPoint) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        if (!checkAuth(request)) {
            throw new UnauthorizedException();
        }
    }
}
