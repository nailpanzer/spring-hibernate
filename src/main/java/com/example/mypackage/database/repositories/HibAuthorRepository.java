package com.example.mypackage.database.repositories;

import com.example.mypackage.database.tables.AuthorTable;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HibAuthorRepository {
    @Autowired
    private SessionFactory sessionFactory;

    public void insert(AuthorTable author) {
        try (Session session = sessionFactory.openSession()) {
            Transaction t = session.beginTransaction();

            session.persist(author);

            t.commit();
        }
    }

    public List<AuthorTable> getAll() {
        List<AuthorTable> authors;

        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<AuthorTable> criteriaQuery = builder.createQuery(AuthorTable.class);
            Root<AuthorTable> root = criteriaQuery.from(AuthorTable.class);

            criteriaQuery.select(root);

            authors = session.createQuery(criteriaQuery).getResultList();
        }

        return authors;
    }

}
