package com.example.mypackage.database.repositories;

import com.example.mypackage.database.tables.AuthorTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Repository with a SpringData jpa query
 */
@Repository
public interface JpaAuthorRepository extends JpaRepository<AuthorTable, Long> {
    @Query("SELECT a FROM authors a WHERE a.name=:author_name")
    List<AuthorTable> findByName(@Param("author_name") String name);
}
