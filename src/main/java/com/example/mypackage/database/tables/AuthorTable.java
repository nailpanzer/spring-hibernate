package com.example.mypackage.database.tables;


import jakarta.persistence.*;

import java.util.Calendar;
import java.util.Date;

@Entity(name = "authors")
@Table(name = "authors")
public class AuthorTable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @Column
    private String name;
    @Temporal(TemporalType.DATE)
    private Date createdDate;

    public AuthorTable() {
    }

    public AuthorTable(String name) {
        this.name = name;
        this.createdDate = Calendar.getInstance().getTime();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AuthorTable{" + "id=" + id + ", name='" + name + '\'' + ", createdDate=" + createdDate + '}';
    }
}
