package com.example.mypackage.main;

import com.example.mypackage.database.tables.AuthorTable;
import com.example.mypackage.kafka.MyKafkaConfig;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;


@Configuration
@ComponentScan(value = "com.example.mypackage")
@EnableAspectJAutoProxy
@EnableJpaRepositories(value = "com.example.mypackage")
@Import({MyKafkaConfig.class})
public class MainConfig {

    @Bean(name = "entityManagerFactory")
    public SessionFactory sessionFactory() {
        org.hibernate.cfg.Configuration cfg = new org.hibernate.cfg.Configuration().configure();
        cfg.addAnnotatedClass(AuthorTable.class);

        StandardServiceRegistry ssRegistry = new StandardServiceRegistryBuilder().applySettings(
                cfg.getProperties()).build();

        return cfg.buildSessionFactory(ssRegistry);
    }

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.sqlite.JDBC");
        dataSourceBuilder.url("jdbc:sqlite:my_db.sqlite");
        return dataSourceBuilder.build();
    }

}
