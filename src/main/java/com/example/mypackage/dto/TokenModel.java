package com.example.mypackage.dto;

public class TokenModel {
    private String token;

    public TokenModel(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
