package com.example.mypackage.dto;

import java.util.Objects;

public final class LoginRequest {
    private final String login;
    private final String password;

    public LoginRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String login() {
        return login;
    }

    public String password() {
        return password;
    }

}
