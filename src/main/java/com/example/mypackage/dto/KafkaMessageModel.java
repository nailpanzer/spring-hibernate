package com.example.mypackage.dto;

import java.io.Serializable;

public class KafkaMessageModel implements Serializable {
    private String message;
    private String message2;

    public KafkaMessageModel(String message, String message2) {
        this.message = message;
        this.message2 = message2;
    }

    public KafkaMessageModel(String message) {
        this(message, "message 2");
    }

    public KafkaMessageModel() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage2() {
        return message2;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    @Override
    public String toString() {
        return "KafkaMessageModel{" +
                "message='" + message + '\'' +
                ", message2='" + message2 + '\'' +
                '}';
    }
}
