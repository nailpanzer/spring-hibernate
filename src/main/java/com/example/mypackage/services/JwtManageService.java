package com.example.mypackage.services;

import com.example.mypackage.dto.UserDetailModel;
import com.example.mypackage.exceptions.UnauthorizedException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Calendar;

@Service
public class JwtManageService {
    private final SecretKey key = Keys.hmacShaKeyFor("mysecretkeymysecretkeymysecretkeymysecretkey".getBytes());

    public String createToken(UserDetailModel userDetail) {
        JwtBuilder jwtBuilder = Jwts.builder();

        jwtBuilder.subject(userDetail.getId());
        jwtBuilder.claim("name", userDetail.getName());
        jwtBuilder.claim("email", userDetail.getEmail());

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        jwtBuilder.expiration(calendar.getTime());

        jwtBuilder.signWith(key);


        System.out.println(encode(jwtBuilder.compact()).toString());

        return jwtBuilder.compact();
    }

    public UserDetailModel encode(String token) {
        try {
            JwtParser parser = Jwts.parser().verifyWith(key).build();
            Claims payload = (Claims) parser.parse(token).getPayload();

            return new UserDetailModel(payload.getSubject(), payload.get("name", String.class),
                    payload.get("email", String.class));
        } catch (JwtException e) {
            throw new UnauthorizedException(e);
        }
    }

    public boolean validate(String token) {
        try {
            JwtParser parser = Jwts.parser().verifyWith(key).build();
            parser.parse(token);
        } catch (JwtException e) {
            return false;
        }
        return true;
    }

}
