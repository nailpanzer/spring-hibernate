package com.example.mypackage.abstractions;

import jakarta.servlet.http.HttpServletRequest;

public interface HttpSecurityStrategy {
    boolean checkAuth(HttpServletRequest request);
}
